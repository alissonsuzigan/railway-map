import React from 'react';
import ReactDOM from 'react-dom';
import './assets/styles/global.scss';
import App from './components/app';
import * as serviceWorker from './serviceWorker';
import railways from './assets/data/railway-data.json';

ReactDOM.render(<App railways={railways} />, document.getElementById('railway-map'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
