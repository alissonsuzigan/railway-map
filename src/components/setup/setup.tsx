import React, { useState, FormEvent, ChangeEvent } from 'react';
import { IConfig, IRailway } from '../../types';

const Setup = ({ railways, setConfigs }: { railways: IRailway[]; setConfigs: Function }) => {
  const [inputs, setInputs]: [IConfig, Function] = useState({
    lineA: 100,
    lineB: 200,
    lineC: 300
  });

  const onChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const { id, value } = event.target;
    setInputs({ ...inputs, [id]: Number(value) });
  };

  const onSubmit = (event: FormEvent): void => {
    event.preventDefault();
    setConfigs(inputs);
  };

  return (
    <div className="setup">
      <h1>Enter the number of passengers of each train:</h1>
      <form className="setup-form" onSubmit={onSubmit}>
        {railways.map((line: IRailway) => (
          <div key={line.id} className="form-group">
            <label htmlFor={line.id}>{line.name}: </label>
            <input type="number" id={line.id} value={inputs[line.id]} onChange={onChange} required />
          </div>
        ))}
        <button className="setup-submit" type="submit">
          Save
        </button>
      </form>
    </div>
  );
};

export default Setup;
