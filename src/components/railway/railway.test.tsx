import React from 'react';
import { render, cleanup } from 'react-testing-library';
import Railway from './railway';
import railways from '../../assets/data/railway-data.json';

const train = {
  currentStation: 0,
  direction: 'FRONT' as 'FRONT',
  id: 'lineA',
  lastStation: 6,
  nextStation: 1,
  passengers: 100
};
const isActive = true;

afterEach(cleanup);

describe('Railway component', () => {
  it('renders properly', () => {
    const { container } = render(<Railway line={railways[0]} train={train} isActive={isActive} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('renders the railway name specified on railway data', () => {
    const { getByText } = render(<Railway line={railways[0]} train={train} isActive={isActive} />);
    expect(getByText(railways[0].name)).toBeDefined();
  });

  it('renders the number of stations specified on railway data', () => {
    const { container } = render(<Railway line={railways[0]} train={train} isActive={isActive} />);
    expect(container.querySelectorAll('.station-item')).toHaveLength(railways[0].stations.length);
  });

  it('renders the paths specified on railway data', () => {
    const { container } = render(<Railway line={railways[0]} train={train} isActive={isActive} />);
    expect(container.querySelectorAll('svg line')).toHaveLength(railways[0].stations.length - 1);
  });
});
