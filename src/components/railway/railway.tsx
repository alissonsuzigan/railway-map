import React, { Fragment } from 'react';
import { getLabelProps, getLineProps, getStationProps } from './helpers';
import { IRailway, ITrain } from '../../types';

const Railway = ({ line, train, isActive }: { line: IRailway; train: ITrain; isActive: boolean }) => {
  const labelProps = getLabelProps(line.stations[0]);
  const lineProps = getLineProps(line.stations, train, isActive);
  const stationProps = getStationProps(line.stations, train, isActive);

  return (
    <Fragment>
      <div className={`stations ${line.id}`} style={{ color: line.color, background: line.color }}>
        {stationProps.map((props, idx) => (
          <div key={idx} {...props} />
        ))}
      </div>
      <svg className={`railway ${line.id}`} fill={line.color} stroke={line.color}>
        <text {...labelProps}>{line.name}</text>
        {lineProps.map((props, idx) => (
          <line key={idx} {...props} />
        ))}
      </svg>
    </Fragment>
  );
};

export default Railway;
