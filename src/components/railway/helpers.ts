import classnames from 'classnames';
import { IStation, ITrain } from '../../types';

export const getLabelProps = (station: IStation) => ({
  x: station.coords[0] - 30,
  y: station.coords[1] - 30
});

export const getLineProps = (stations: IStation[], train: ITrain, isActive: boolean) => {
  const { direction, currentStation, nextStation } = train;
  const paths = [];
  let i = 0;
  const len = stations.length - 1;
  for (i; i < len; i++) {
    const coords = {
      x1: stations[i].coords[0],
      y1: stations[i].coords[1],
      x2: stations[i + 1].coords[0],
      y2: stations[i + 1].coords[1],
      className: classnames({
        'move-front': direction === 'FRONT' && currentStation === i && currentStation !== nextStation && isActive,
        'move-back': direction === 'BACK' && nextStation === i && currentStation !== nextStation && isActive
      })
    };
    paths.push(coords);
  }
  return paths;
};

export const getStationProps = (stations: IStation[], train: ITrain, isActive: boolean) =>
  stations.map((station: IStation, idx) => ({
    style: {
      left: `${station.coords[0]}px`,
      top: `${station.coords[1]}px`
    },
    className: classnames('station-item', {
      'is-crossing': station.isCrossing,
      active: train.currentStation === idx,
      blink: train.nextStation === idx && isActive
    })
  }));
