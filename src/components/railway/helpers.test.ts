import { getLabelProps, getLineProps, getStationProps } from './helpers';
import { ITrain } from '../../types';

// Mocks
const singleStation = {
  name: 'Station test',
  coords: [130, 130],
  isCrossing: false
};

const stations = [
  {
    name: 'Station 1',
    coords: [100, 100],
    isCrossing: false
  },
  {
    name: 'Station 2',
    coords: [200, 200],
    isCrossing: false
  }
];

let train: ITrain;

beforeEach(() => {
  train = {
    currentStation: 0,
    direction: 'FRONT' as 'FRONT' | 'BACK',
    id: 'lineA',
    lastStation: 6,
    nextStation: 1,
    passengers: 100
  };
});

// Tests
describe('Railway helper', () => {
  it('gets the label properties when `getLabelProps` is called', () => {
    const expected = { x: 100, y: 100 };
    expect(getLabelProps(singleStation)).toEqual(expected);
  });

  it('gets the front line properties when `getLineProps` is called', () => {
    const expected = [{ className: 'move-front', x1: 100, y1: 100, x2: 200, y2: 200 }];
    expect(getLineProps(stations, train, true)).toEqual(expected);
  });

  it('gets the back line properties when `getLineProps` is called', () => {
    train.direction = 'BACK' as 'BACK';
    train.currentStation = 1;
    train.nextStation = 0;

    const expected = [{ className: 'move-back', x1: 100, y1: 100, x2: 200, y2: 200 }];
    expect(getLineProps(stations, train, true)).toEqual(expected);
  });

  it('gets the station properties when `getStationProps` is called', () => {
    const expected = [
      {
        className: 'station-item active',
        style: { left: '100px', top: '100px' }
      },
      {
        className: 'station-item blink',
        style: { left: '200px', top: '200px' }
      }
    ];
    expect(getStationProps(stations, train, true)).toEqual(expected);
  });
});
