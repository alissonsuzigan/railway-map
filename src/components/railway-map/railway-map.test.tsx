import React from 'react';
import { act } from 'react-dom/test-utils';
import { render, cleanup } from 'react-testing-library';
import RailwayMap from './railway-map';
import railways from '../../assets/data/railway-data.json';

const configs = { lineA: 100, lineB: 200, lineC: 300 };
afterEach(cleanup);

describe('RailwayMap component', () => {
  it('renders properly', () => {
    const { container } = render(<RailwayMap railways={railways} configs={configs} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('renders the number of railways specified on railway data', () => {
    const { container } = render(<RailwayMap railways={railways} configs={configs} />);
    expect(container.querySelectorAll('svg')).toHaveLength(railways.length);
  });

  it('changes activity every 2 seconds', () => {
    jest.useFakeTimers();
    let { container } = render(<RailwayMap railways={railways} configs={configs} />);
    expect(container.querySelectorAll('.blink')).toHaveLength(0);

    act(() => {
      jest.advanceTimersByTime(2000);
    });
    expect(container.querySelectorAll('.blink')).toHaveLength(3);

    act(() => {
      jest.advanceTimersByTime(2000);
    });
    expect(container.querySelectorAll('.blink')).toHaveLength(0);
  });
});
