import React from 'react';
import { render, fireEvent, cleanup } from 'react-testing-library';
import Setup from './setup';
import railways from '../../assets/data/railway-data.json';

describe('Setup component', () => {
  const setConfigs = jest.fn();

  afterEach(cleanup);

  it('renders properly', () => {
    const { container } = render(<Setup railways={railways} setConfigs={setConfigs} />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it('renders a input element for each railway', () => {
    const setup = render(<Setup railways={railways} setConfigs={setConfigs} />);
    expect(setup.getAllByLabelText(/Line/)).toHaveLength(railways.length);
  });

  it('calls the method `setConfigs` when the form was submited', () => {
    const expected = { lineA: 100, lineB: 200, lineC: 300 };
    const { getByText } = render(<Setup railways={railways} setConfigs={setConfigs} />);
    fireEvent.click(getByText('Save'));
    expect(setConfigs).toHaveBeenCalled();
    expect(setConfigs).toHaveBeenCalledWith(expected);
  });

  it('changes the first input value', () => {
    const { getByLabelText } = render(<Setup railways={railways} setConfigs={setConfigs} />);
    const firstInput = getByLabelText('Line A:');
    const newValue = '210';
    fireEvent.change(firstInput, { target: { value: newValue } });
    expect(firstInput.value).toBe(newValue);
  });
});
