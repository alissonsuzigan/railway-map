import React, { useReducer } from 'react';
import Railway from '../railway';
import { setupTrain, reducer, INITIAL_STATE, MOVE_TRAIN, updateTrain } from '../../store';
import { IRailway, IConfig, IState } from '../../types';

const RailwayMap = ({ railways, configs }: { railways: IRailway[]; configs: IConfig }) => {
  const init = () => setupTrain(railways, configs);
  const [state, dispatch] = useReducer(reducer, INITIAL_STATE, init);

  const changeActivity = (state: IState): void => {
    if (state.isActive) dispatch(updateTrain(railways, state.trains));
    else dispatch({ type: MOVE_TRAIN });
  };

  setTimeout(() => {
    changeActivity(state);
  }, 2000);

  return (
    <div className="map">
      <h1>Railway Map System</h1>
      <div className="map-lines">
        {railways.map((line: IRailway, idx: number) => (
          <Railway key={line.id} line={line} train={state.trains[idx]} isActive={state.isActive || false} />
        ))}
      </div>
    </div>
  );
};

export default RailwayMap;
