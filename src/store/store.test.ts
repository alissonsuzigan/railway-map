import { setupTrain, moveTrain, updateTrain, reducer, MOVE_TRAIN, UPDATE_TRAIN, goNextStation } from '.';
import rwData from '../assets/data/railway-data.json';
import { ITrain } from '../types';

// Mocks
const configs = { lineA: 100, lineB: 200, lineC: 300 };
let trains: ITrain[];

beforeEach(() => {
  trains = [
    {
      id: 'lineA',
      currentStation: 0,
      nextStation: 1,
      lastStation: 6,
      direction: 'FRONT' as 'FRONT' | 'BACK',
      passengers: 100
    },
    {
      id: 'lineB',
      currentStation: 0,
      nextStation: 1,
      lastStation: 7,
      direction: 'FRONT' as 'FRONT' | 'BACK',
      passengers: 200
    }
  ];
});

// Actions
describe('Action creators', () => {
  it('`setupTrain` returns the train initial configs', () => {
    const expected = {
      trains: [
        {
          currentStation: 0,
          direction: 'FRONT' as 'FRONT',
          id: 'lineA',
          lastStation: 6,
          nextStation: 1,
          passengers: 100
        }
      ]
    };
    const setup = setupTrain(rwData, { lineA: 100 });
    expect(setup).toEqual(expected);
  });

  it('`moveTrain` returns MOVE_TRAIN action type', () => {
    expect(moveTrain()).toEqual({ type: MOVE_TRAIN });
  });

  it('`updateTrain` updates the application state', () => {
    const response = updateTrain(rwData, trains);
    const expected = [
      {
        id: 'lineA',
        currentStation: 1,
        nextStation: 1,
        lastStation: 6,
        direction: 'FRONT' as 'FRONT',
        passengers: 100
      },
      {
        id: 'lineB',
        currentStation: 1,
        nextStation: 2,
        lastStation: 7,
        direction: 'FRONT' as 'FRONT',
        passengers: 200
      }
    ];
    expect(response.type).toBe(UPDATE_TRAIN);
    expect(response.payload).toEqual({ trains: expected });
  });
});

// Reducer
describe('Reducer action type', () => {
  it('`SETUP_TRAIN`, updates the state with the initial configs', () => {
    const action = { type: 'SETUP_TRAIN' as 'SETUP_TRAIN', payload: { trains } };
    const result = reducer(undefined, action);
    const expected = {
      isActive: false,
      trains: [
        { currentStation: 0, direction: 'FRONT', id: 'lineA', lastStation: 6, nextStation: 1, passengers: 100 },
        { currentStation: 0, direction: 'FRONT', id: 'lineB', lastStation: 7, nextStation: 1, passengers: 200 }
      ]
    };
    expect(result).toEqual(expected);
  });

  it('`MOVE_TRAIN`, updates the `isActive` property state', () => {
    const action = { type: 'MOVE_TRAIN' as 'MOVE_TRAIN' };
    const result = reducer(undefined, action);
    const expected = { isActive: true, trains: [] };
    expect(result).toEqual(expected);
  });

  it('`UPDATE_TRAIN`, updates the state with the initial configs', () => {
    const action = { type: 'UPDATE_TRAIN' as 'UPDATE_TRAIN' };
    const result = reducer(undefined, action);
    const expected = { isActive: false, trains: [] };
    expect(result).toEqual(expected);
  });
});

// Helpers
describe('Helpers method `goNextStation`', () => {
  it('updates trains moving to Front', () => {
    const result = goNextStation(trains);
    const expected = trains.map(train => {
      (train.currentStation = train.currentStation + 1), (train.nextStation = train.nextStation + 1);
      return train;
    });
    expect(result).toEqual(expected);
  });

  it('updates trains in last station moving to Front', () => {
    const newTrain = trains.map(train => {
      train.currentStation = train.lastStation - 1;
      train.nextStation = train.lastStation;
      return train;
    });

    const result = goNextStation(newTrain);
    const expected = trains.map(train => {
      train.direction = 'BACK';
      train.currentStation = train.lastStation;
      train.nextStation = train.nextStation - 1;
      return train;
    });
    expect(result).toEqual(expected);
  });

  it('updates trains moving to Back', () => {
    const newTrain = trains.map(train => {
      train.direction = 'BACK';
      train.currentStation = 4;
      train.nextStation = 3;
      return train;
    });
    const result = goNextStation(newTrain);
    const expected = trains.map(train => {
      (train.currentStation = train.currentStation - 1), (train.nextStation = train.nextStation - 1);
      return train;
    });
    expect(result).toEqual(expected);
  });

  it('updates trains in the first station moving to Back', () => {
    const newTrain = trains.map(train => {
      train.direction = 'BACK';
      train.currentStation = 1;
      train.nextStation = 0;
      return train;
    });
    const result = goNextStation(newTrain);
    const expected = trains.map(train => {
      train.direction = 'FRONT';
      train.currentStation = train.currentStation - 1;
      train.nextStation = train.nextStation + 1;
      return train;
    });
    expect(result).toEqual(expected);
  });
});
