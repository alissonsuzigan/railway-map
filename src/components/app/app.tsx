import React, { useState } from 'react';
import Setup from '../setup';
import RailwayMap from '../railway-map';
import { IRailway, IConfig } from '../../types';

const App = ({ railways, config }: { railways: IRailway[]; config?: IConfig }) => {
  if (railways.length === 0) {
    return <p className="error">WARNING: No railway data was found!</p>;
  }

  const [configs, setConfigs] = useState(config);
  return configs ? (
    <RailwayMap railways={railways} configs={configs} />
  ) : (
    <Setup railways={railways} setConfigs={setConfigs} />
  );
};

export default App;
