import React from 'react';
import ReactDOM from 'react-dom';
import { render, cleanup } from 'react-testing-library';
import App from '.';
import railways from '../../assets/data/railway-data.json';

afterEach(cleanup);

describe('App component', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App railways={railways} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders the Setup component title', () => {
    const { getByText } = render(<App railways={railways} />);
    expect(getByText(/Enter the number/)).toBeDefined();
  });

  it('renders the RailwayMap component title', () => {
    const config = { lineA: 100, lineB: 200, lineC: 300 };
    const { getByText } = render(<App railways={railways} config={config} />);
    expect(getByText(/Railway Map/)).toBeDefined();
  });

  it('renders the error message', () => {
    const { getByText } = render(<App railways={[]} />);
    expect(getByText(/WARNING:/)).toBeDefined();
  });
});
