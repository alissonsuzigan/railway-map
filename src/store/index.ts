import { IConfig, ITrain, IRailway, IState, IAction } from '../types';

// Action types
export const SETUP_TRAIN = 'SETUP_TRAIN';
export const MOVE_TRAIN = 'MOVE_TRAIN';
export const UPDATE_TRAIN = 'UPDATE_TRAIN';

// Train directions
export const FRONT = 'FRONT';
export const BACK = 'BACK';

// Action creators
export const setupTrain = (rwData: IRailway[], configs: IConfig): IState => {
  const trains: ITrain[] = [];
  Object.keys(configs).forEach((key, idx) => {
    trains.push({
      id: key,
      currentStation: 0,
      nextStation: 1,
      lastStation: rwData[idx].stations.length - 1,
      passengers: configs[key],
      direction: FRONT
    });
  });
  return { trains };
};

export const moveTrain = (): IAction => {
  return { type: MOVE_TRAIN };
};

export const updateTrain = (rwData: IRailway[], trains: ITrain[]): IAction => {
  const changedTrains = resolveCollision(rwData, goNextStation(trains));

  return {
    type: UPDATE_TRAIN,
    payload: { trains: changedTrains }
  };
};

// Initial state
export const INITIAL_STATE: IState = {
  isActive: true,
  trains: []
};

// Reducer
export const reducer = (state = INITIAL_STATE, action: IAction): IState => {
  switch (action.type) {
    case SETUP_TRAIN:
      return {
        ...state,
        ...action.payload,
        isActive: false
      };

    case MOVE_TRAIN:
      return {
        ...state,
        isActive: true
      };

    case UPDATE_TRAIN:
      return {
        ...state,
        ...action.payload,
        isActive: false
      };
  }
};

// Helper methods to change stations
export const goNextStation = (trains: ITrain[]): ITrain[] => {
  return trains.map((train: ITrain) => {
    return train.direction === FRONT ? goFront(train) : goBack(train);
  });
};

export const goFront = (train: ITrain): ITrain => {
  if (train.nextStation < train.lastStation) {
    return {
      ...train,
      currentStation: train.nextStation,
      nextStation: train.nextStation + 1
    };
  }
  return {
    ...train,
    direction: BACK,
    currentStation: train.lastStation,
    nextStation: train.lastStation - 1
  };
};

export const goBack = (train: ITrain): ITrain => {
  if (train.nextStation > 0) {
    return {
      ...train,
      currentStation: train.nextStation,
      nextStation: train.nextStation - 1
    };
  }
  return {
    ...train,
    direction: FRONT,
    currentStation: 0,
    nextStation: 1
  };
};

// Helper methods to resolve collisions
export const resolveCollision = (rwData: IRailway[], trains: ITrain[]): ITrain[] => {
  let changedTrains = [...trains];
  const nextCoords = getNextCoords(rwData, trains);
  const collisionIds = checkCollisionIds(nextCoords);
  if (collisionIds.length) {
    changedTrains = setPriority(collisionIds, changedTrains);
    resolveCollision(rwData, changedTrains);
  }
  return changedTrains;
};

export const setPriority = (collisionIds: number[], trains: ITrain[]) => {
  const changedTrains = [...trains];
  const collisionPassengers = collisionIds.map(id => changedTrains[id].passengers);
  const waitingTrainId = collisionPassengers.indexOf(Math.min(...collisionPassengers));
  changedTrains[collisionIds[waitingTrainId]].nextStation = changedTrains[collisionIds[waitingTrainId]].currentStation;
  return changedTrains;
};

export const getNextCoords = (rwData: IRailway[], trains: ITrain[]): string[] => {
  return trains.map((train, idx) => rwData[idx].stations[train.nextStation].coords.join('-'));
};

const checkCollisionIds = (coords: string[]): number[] => {
  let collisionIds: number[] = [];
  coords.forEach(item => {
    if (coords.indexOf(item) !== coords.lastIndexOf(item)) {
      collisionIds = [coords.indexOf(item), coords.lastIndexOf(item)];
    }
  });
  return collisionIds;
};
