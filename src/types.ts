export interface IConfig {
  [prop: string]: number;
}

export interface IStation {
  name: string;
  coords: number[];
  isCrossing: boolean;
}

export interface IRailway {
  id: string;
  name: string;
  color: string;
  stations: IStation[];
}

export interface ITrain {
  id: string;
  currentStation: number;
  nextStation: number;
  lastStation: number;
  passengers: number;
  direction: 'FRONT' | 'BACK';
}

export interface IState {
  isActive?: boolean;
  trains: ITrain[];
}

export interface IAction {
  type: 'SETUP_TRAIN' | 'MOVE_TRAIN' | 'UPDATE_TRAIN';
  payload?: IState;
}
